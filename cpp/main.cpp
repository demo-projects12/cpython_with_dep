#include <pybind11/pybind11.h>
#include "my_test_f.h"

PYBIND11_MODULE(cpython_with_dep, m) {
    m.def(
        "my_test_func",
        &my_test_func
    );
};
