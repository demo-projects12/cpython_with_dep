import os
import pathlib
import re
import shutil
import site
import subprocess
import sys
from sys import platform

import numpy
from setuptools import Extension, find_packages, setup
from setuptools.command.build_ext import build_ext

LIBS_TO_INSTALL = "libs_to_install"

EXTRA_CMAKE_BUILD_ARGS = "--target cunitops"

# Convert distutils Windows platform specifiers to CMake -A arguments
PLAT_TO_CMAKE = {
    "win32": "Win32",
    "win-amd64": "x64",
}


# A CMakeExtension needs a sourcedir instead of a file list.
# The name must be the _single_ output extension from the CMake build.
# If you need multiple extensions, see scikit-build.
class CMakeExtension(Extension):
    def __init__(self, name, sourcedir=""):
        Extension.__init__(self, name, sources=[])
        self.sourcedir = os.path.abspath(sourcedir)


def split_cmake_args():
    cmake_arg_prefix = "-D"
    if "CMAKE_ARGS" in os.environ:
        return [
            cmake_arg_prefix + e
            for e in os.environ["CMAKE_ARGS"].split(cmake_arg_prefix)
            if e
        ]
    return []


class CMakeBuild(build_ext):
    def build_extension(self, ext):
        extdir = os.path.abspath(os.path.dirname(self.get_ext_fullpath(ext.name)))

        # required for auto-detection & inclusion of auxiliary "native" libs
        if not extdir.endswith(os.path.sep):
            extdir += os.path.sep

        debug = int(os.environ.get("DEBUG", 0)) if self.debug is None else self.debug
        cfg = "Debug" if debug else "Release"

        # CMake lets you override the generator - we need to check this.
        # Can be set with Conda-Build, for example.
        cmake_generator = os.environ.get("CMAKE_GENERATOR", "")

        # Set Python_EXECUTABLE instead if you use PYBIND11_FINDPYTHON
        # EXAMPLE_VERSION_INFO shows you how to pass a value into the C++ code
        # from Python.
        cmake_args = [
            f"-DCMAKE_LIBRARY_OUTPUT_DIRECTORY={extdir}",
            f"-DPYTHON_EXECUTABLE={sys.executable}",
            f"-DCMAKE_BUILD_TYPE={cfg}",  # not used on MSVC, but no harm
        ]
        build_args = []

        # Adding CMake arguments set as environment variable
        # (needed e.g. to build for ARM OSx on conda-forge)
        cmake_args += split_cmake_args()

        # Specify the arch if using MSVC generator, but only if it doesn't
        # contain a backward-compatibility arch spec already in the
        # generator name.
        if "Visual Studio" in cmake_generator:
            contains_arch = any(x in cmake_generator for x in {"ARM", "Win64"})
            if not contains_arch:
                cmake_args += ["-A", PLAT_TO_CMAKE[self.plat_name]]
                cmake_args += [
                    f"-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_{cfg.upper()}={extdir}"
                ]
                build_args += ["--config", cfg]

        if sys.platform.startswith("darwin"):
            # Cross-compile support for macOS - respect ARCHFLAGS if set
            archs = re.findall(r"-arch (\S+)", os.environ.get("ARCHFLAGS", ""))
            if archs:
                cmake_args += ["-DCMAKE_OSX_ARCHITECTURES={}".format(";".join(archs))]

        # Set CMAKE_BUILD_PARALLEL_LEVEL to control the parallel build level
        # across all generators.
        if "CMAKE_BUILD_PARALLEL_LEVEL" not in os.environ:
            # self.parallel is a Python 3 only way to set parallel jobs by hand
            # using -j in the build_ext call, not supported by pip or PyPA-build.
            if hasattr(self, "parallel") and self.parallel:
                # CMake 3.12+ only.
                build_args += [f"-j{self.parallel}"]

        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)

        build_args += EXTRA_CMAKE_BUILD_ARGS.split()

        if cmake_generator:
            cmake_args += ["-G", cmake_generator]

        subprocess.check_call(
            ["cmake", ext.sourcedir] + cmake_args, cwd=self.build_temp
        )
        subprocess.check_call(
            ["cmake", "--build", "."] + build_args, cwd=self.build_temp
        )


with open("requirements.txt") as fp:
    install_requires = fp.read().splitlines()


def check_openmp_supported():
    """
    Проверка поддержки компилятором директив openmp.

    Если не поддерживается, из CMakeLists.txt в субпроцессе
    будет вызвано исключение.
    """
    current_dir = pathlib.Path(__file__).parent.resolve()
    build_dir = os.path.join(current_dir, "build_check_omp_support")
    shutil.rmtree(build_dir, ignore_errors=True)
    os.makedirs(build_dir, exist_ok=True)
    build_dir_arg = f"-B{build_dir}"
    extern_cmake_args = split_cmake_args()

    try:
        subprocess.check_call(
            ["cmake", ".", build_dir_arg] + extern_cmake_args, cwd=current_dir
        )
    finally:
        shutil.rmtree(build_dir)


def search_file(rootdir, regex_str):
    files_paths = []
    pattern = re.compile(regex_str)
    for root, directories, files in os.walk(rootdir):
        for file in files:
            if pattern.match(file):
                files_paths.append(os.path.join(root, file))
    return files_paths


def get_libomp_abs_path() -> str:
    if "LIBOMP_PATH" in os.environ:
        if not os.path.exists(os.environ["LIBOMP_PATH"]):
            raise FileNotFoundError(
                f"Given libomp path does not exist "
                f"(\"{os.environ['LIBOMP_PATH']}\")"
            )
        return os.environ["LIBOMP_PATH"]
    elif platform == "linux":
        print("Search for libomp dynamic library in standart paths...")
        libpath = search_file("/usr", "^lib?.omp\.so$")
        print(f"Found libpath = {libpath}")
        if not libpath:
            check_openmp_supported()
            raise FileNotFoundError(
                "Openmp dynamic library is required but not found. "
                'Please specify "LIBOMP_PATH" environment variable.'
            )

        if len(libpath) != 1:
            raise FileNotFoundError(
                f"More than one openmp dynamic library files found."
                'Please select correct library path via "LIBOMP_PATH" '
                "environment variable. "
            )
        return libpath[0]
    else:
        raise FileNotFoundError(
            "Openmp dynamic library is required but not found. "
            'Please specify "LIBOMP_PATH" environment variable.'
        )


def get_libs_to_install(libs_dir: str) -> list:
    """
    Вернуть список сторонних библиотек, предназначенных для установки.

    Parameters
    ----------
    libs_dir: str
        Каталог с библиотеками относительно setup.py
    Returns
    -------
    list of str
    """
    check_env_definitions_consistency()
    # if platform == "win32":
    #     local_dir = os.path.join(pathlib.Path(__file__).parent.resolve(), libs_dir)
    #     shutil.rmtree(local_dir, ignore_errors=True)
    #     os.makedirs(local_dir)
    #     libomp_path = get_libomp_abs_path()
    #     shutil.copy(libomp_path, local_dir)
    #     print(
    #         f"libs to install: {[os.path.join(libs_dir, path) for path in os.listdir(local_dir)]}"
    #     )
    #     return [os.path.join(libs_dir, path) for path in os.listdir(local_dir)]
    # else:
    #     return []
    local_dir = os.path.join(pathlib.Path(__file__).parent.resolve(), libs_dir)
    shutil.rmtree(local_dir, ignore_errors=True)
    os.makedirs(local_dir)
    libomp_path = get_libomp_abs_path()
    shutil.copy(libomp_path, local_dir)
    print(
        f"libs to install: {[os.path.join(libs_dir, path) for path in os.listdir(local_dir)]}"
    )
    return [os.path.join(libs_dir, path) for path in os.listdir(local_dir)]


def get_site_packages_rel_path() -> str:
    """
    Вычислить относительный путь для site-packages.

    Returns
    -------
    str
        Путь к site-packages относительно корневого каталога
        виртуального окружения.
    """
    if platform == "win32":
        return os.path.join("libs", "site-packages")
    elif platform == "linux" or platform == "darwin":
        sp_path = re.search(r"lib\/python\d\.\d\/site-packages", numpy.__file__)
        return sp_path[0]


def check_env_definitions_consistency():
    compiler_defs = ["CMAKE_CXX_COMPILER", "CMAKE_C_COMPILER", "CMAKE_MAKE_PROGRAM"]
    defined_compiler_vars = []

    # defined_compiler_vars = (compiler_defs & set(os.environ))
    if "CMAKE_ARGS" in os.environ:
        defined_compiler_vars = [df in os.environ["CMAKE_ARGS"] for df in compiler_defs]

    # Переменные для компилятора могут быть заданы только вместе либо вообще не определены
    if defined_compiler_vars and not all(defined_compiler_vars):
        raise EnvironmentError(
            f"All of these environment vars should be set altogether: {compiler_defs}"
        )

    # Если явно заданы пути к компилятору, нужно определить переменную 'CMAKE_GENERATOR'
    if defined_compiler_vars and "CMAKE_GENERATOR" not in os.environ:
        raise EnvironmentError(
            f"CMAKE_GENERATOR environment var should be set "
            f"with non-default compiler paths"
        )

    if platform == "win32":
        # Если переменные для компилятора заданы, то нужно определить и LIBOMP_PATH,
        # но LIBOMP_PATH может быть задана и без определения переменных компилятора.
        if defined_compiler_vars and "LIBOMP_PATH" not in os.environ:
            raise EnvironmentError(
                "With non-default compiler paths LIBOMP_PATH also required."
            )


setup(
    name="unitops",
    version="0.1.0",
    author="Ivan Babushkin, Artem Ermulin",
    ext_modules=[CMakeExtension("cunitops")],
    cmdclass={"build_ext": CMakeBuild},
    zip_safe=False,
    packages=find_packages(),
    install_requires=install_requires,
    include_package_data=True,
    data_files=[(get_site_packages_rel_path(), get_libs_to_install(LIBS_TO_INSTALL))],
    runtime_library_dirs=[site.getsitepackages()[-1]],
)


shutil.rmtree(LIBS_TO_INSTALL, ignore_errors=True)
